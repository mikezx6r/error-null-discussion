package ca.mikewilkes.nullhandling;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class NullDemo {

    // No way for caller to know what fields are mandatory, nor those that are optional, therefore
    // method has to be defensive
    //
    public static String formatName(String firstName, String middleName, String lastName) {
        if (firstName == null || lastName == null) {
            throw new RuntimeException("Firstname, and lastname are required fields");
        }

        if (middleName == null) {
            return firstName + " " + lastName;
        } else {
            return firstName + " " + middleName + " " + lastName;
        }
    }

    // Could write this way. As you can see, not recommended code style/usage, and can cause issues
    // Overhead of boxing
    // 'messier' code
    public static String formatNameO(String firstName, Optional<String> middleName, String lastName) {
        return firstName + " " + middleName.map(it -> it + " ").orElse("") + lastName;
    }

    // Can add annotations, but only if dev pays close attention to IDE warnings, and processing
    // Of course IJ now tells me my 'if' will always be true, BUT it can't truly guarantee that
    public static String formatNameA(@NotNull String firstName, @Nullable String middleName, @NotNull String lastName) {
        if (firstName == null || lastName == null) {
            throw new RuntimeException("Firstname, and lastname are required fields");
        }

        if (middleName == null) {
            return firstName + " " + lastName;
        } else {
            return firstName + " " + middleName + " " + lastName;
        }
    }

    // With signature like this, no way to know if it will always return a value, so caller has to be defensive
    public static String findPerson(String firstName, String middleName) {
        if (firstName.equals("Fred")) {
            return "Fred";
        } else {
            return null;
        }
    }

    // At least now caller knows it's null, and has to handle it.
    public static Optional<String> findPersonO(String firstName, String middleName) {
        if (firstName.equals("Fred")) {
            return Optional.of("Fred");
        } else {
            return Optional.empty();
        }
    }


}
