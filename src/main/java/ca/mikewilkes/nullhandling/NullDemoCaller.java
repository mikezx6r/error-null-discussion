package ca.mikewilkes.nullhandling;

import java.util.Optional;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class NullDemoCaller {

    public static void main(String[] args) {
        NullDemo.formatNameO(null, Optional.of("Test"), null);

        NullDemo.formatNameA(null, "middle", "testValue");


        NullDemo.formatName(null, null, null);


        Optional<String> personO = NullDemo.findPersonO("first", "last");
        personO.orElse("value");
        personO.map(p ->
                // do something with person here
                "new value"
                );
        // consume the value
        personO.ifPresent(System.out::println);
    }
}
