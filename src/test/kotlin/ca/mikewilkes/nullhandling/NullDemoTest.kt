package ca.mikewilkes.nullhandling

import io.kotest.assertions.fail
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class NullDemoTest : FreeSpec({
    "verify formatName" - {
        "should throw runtime exception because of nulls" {
            shouldThrow<RuntimeException> {
                NullDemo.formatName(null, null, null)
            }
        }

        "should format name" {
            NullDemo.formatName("first", null, "last") shouldBe "first last"
        }

        "should format name all parts" {
            NullDemo.formatName("first", "middle", "last") shouldBe "first middle last"
        }
    }

    "verify formatNameA" - {
        "verify can't call with nulls for non-null values" {
            //TODO UNCOMMENT to show won't even compile
//            NullDemo.formatNameA(null, "middle", "last")
        }
    }

    "Kotlin formatName enforcement" - {
        "verify formats name with null value" {
            formatName("first", null, "last") shouldBe "first last"
        }
        "verify formats name with all value" {
            formatName("first", "m", "last") shouldBe "first m last"
        }
    }

    "Find returning optional value" {
        val t = findPerson("some name", "other")

        // compiler tells us that we have a possibly null value, so therefore we must handle it.
        // Have several options here, including ignoring it using !!
        // If use !!, but value is null, it will throw an exception
        shouldThrow<NullPointerException> {
            formatName(t!!, null, "last")
        }

        formatName(t ?: "Mike", null, "Wilkes") shouldBe "Mike Wilkes"

        // If only want to do something if it is not null, although highly recommended to use 'if (t != null)' instead
        t?.let {
            fail("Should never get here in this test")
        }
    }
})