package ca.mikewilkes.nullhandling

fun formatName(firstName: String, middleName: String?, lastName: String): String =
    // overly clever code. Wouldn't recommend this as an actual implementation.
    """$firstName ${middleName?.let { "$it " } ?: ""}$lastName"""

fun findPerson(first: String, last: String): String? = if (first == "Fred") "Fred" else null